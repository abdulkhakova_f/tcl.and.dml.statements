/*in my database film id id 1001*/
UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE film_id = 1001;


UPDATE customer AS c
SET
    first_name = 'Fotima',
    last_name = 'Abdulkhakova',
    email = 'abdulkhakova@gmail.com',
    address_id = 813  -- Change this to the desired address_id
WHERE c.customer_id IN (
    SELECT c.customer_id
    FROM customer AS c
    JOIN (
        SELECT customer_id
        FROM rental
        GROUP BY customer_id
        HAVING COUNT(*) > 10
    ) AS r ON c.customer_id = r.customer_id
    JOIN (
        SELECT customer_id
        FROM payment
        GROUP BY customer_id
        HAVING COUNT(*) > 10
    ) AS p ON c.customer_id = p.customer_id
    LIMIT 10
);

UPDATE customer
SET create_date = CURRENT_DATE;

